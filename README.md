## clone the repository
- open terminal
- go to directory where you want to clone the repository
`$git clone https://gitlab.com/MegaKeegMan/ascii-animation-guide.git`

## Open the HTML document
- enter the cloned repository
- `$open index.html`
