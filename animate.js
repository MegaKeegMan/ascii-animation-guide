// Before pasting ascii frame into html <pre> element, paste into scrap file
// replace all "& with "&amp;"
// replace all "<" with "<"
// for each frame, paste text into <pre> element
// all <pre> elements will be placed inside a div with id="animation"

// use animation sequence to set the order of your frames
// change the number in animation interval adjust the time of the animation interval (in milliseconds)


animationSequence = []
let leftBirdTalks = ["f1", "f2", "f1", "f2","f1", "f2", "f1", "f2"]
let rightBirdTalks = ["f1", "f3", "f1", "f3", "f1", "f3", "f1", "f3"]
let bothBirdsTalk = ["f2", "f3", "f2", "f3", "f2", "f3", "f2", "f3"]
let stillBirds = ["f1", "f1", "f1", "f1"]
let loveBirds = ["f1", "f1", "f4", "f4", "f5", "f5", "f1", "f1", "f4", "f4", "f6", "f6", "f7", "f7"]


animationSequence = animationSequence.concat(leftBirdTalks, rightBirdTalks, bothBirdsTalk, stillBirds, loveBirds, bothBirdsTalk, loveBirds, bothBirdsTalk)
// debugger
// animationSequence = leftBirdTalks + rightBirdTalks
let animationInterval = setInterval(change, 100);


// 1) The below code will automatically create id's for frames so that animators do not have to label them all themselves or reference the frames manually. The naming convention here is f1, f2, f3, etc...

let animation = document.getElementById("animation")
// this is the only place animation is used, but keeping the id on the element could be useful for styling.
let frames = Array.from(animation.children)
frameCount = 1
frames.forEach(frame => {
  if (frame.tagName === "PRE") {
    frame.id = `f${frameCount}`
    frameCount += 1
  }
})


// 2) this is how you would reference the frames id's manually.
// if you wish to do it this way, be sure to label all of your <pre> tags with the appropriate id in your index.html manually also!

// var frame1 = document.getElementById("f1");
// var frame2 = document.getElementById("f2");
// var frames = [frame1, frame2]


// 3) The method you choose will be a matter of personal preference, and may change depending on the details of your animation!

// everything else below is what makes the displayed frame change

let prevVisibleFrameId = "f1"
let prevVisibleFrame = document.getElementById(prevVisibleFrameId)
let counter = 0

function change() {

// checks each frame to see if it has the same id has the next string in the animationSequence. If it does, set the prevVisibleFrame to not display and set the matching frame to display, and then reassigns the frame as prevVisibleFrame
  frames.forEach(frame => {
    if (frame.id === animationSequence[counter]) {
      prevVisibleFrame.style.display = "none"
      frame.style.display = "block"
      prevVisibleFrameId = frame.id
      prevVisibleFrame = document.getElementById(prevVisibleFrameId)
    }
  })
  counter++;
  if (counter >= animationSequence.length) {
    counter = 0;
    // clearInterval(animationInterval); // uncomment this if you want to stop refreshing after one cycle
  }
}


// below is some code that allows the animation to turn on and off when the animation animation div is clicked. It may or may not be very practical, but it can be done!
let animationOn = true
let pauseAndPlayButton = document.getElementById("pauseAndPlayButton")
function pauseAndResume() {
  if (animationOn){
    clearInterval(animationInterval)
    animationOn = false
    pauseAndPlayButton.innerText = ">"
  } else {
    animationInterval = setInterval(change, 100);
    animationOn = true
    pauseAndPlayButton.innerText = "||"
  }

}

pauseAndPlayButton.addEventListener("click", pauseAndResume);
